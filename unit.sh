#!/bin/bash
set -e
cd tests/codeception/api
../../../vendor/bin/codecept run unit $*  -d  --coverage --coverage-html
