#!/bin/bash
php tests/codeception/bin/yii migrate --interactive=0
php tests/codeception/bin/yii migrate --migrationPath=@tests/tests/codeception/common/migrations --interactive=0
cd tests/codeception/api
../../../vendor/bin/codecept run acceptance
