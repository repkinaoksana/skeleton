# Build containers from Dockerfile

## Login
```bash
docker login registry.gitlab.com
```

## Build project cli
```bash
docker build \
    -t registry.gitlab.com/templatemonster/plasmaplatform/services/service-loyalty-programs/php7-cli/loc:latest \
    -t registry.gitlab.com/templatemonster/plasmaplatform/services/service-loyalty-programs/php7-cli/loc:1.0.0 .
```

## Build project fpm
```bash
docker build \
    -t registry.gitlab.com/templatemonster/plasmaplatform/services/service-loyalty-programs/php7-fpm/loc:latest \
    -t registry.gitlab.com/templatemonster/plasmaplatform/services/service-loyalty-programs/php7-fpm/loc:1.0.0 .
```
## Push cli project into gitlab registry
```bash
docker push registry.gitlab.com/templatemonster/plasmaplatform/services/service-loyalty-programs/php7-cli/loc:1.0.0
docker push registry.gitlab.com/templatemonster/plasmaplatform/services/service-loyalty-programs/php7-cli/loc:latest
```
## Push fpm project into gitlab registry
```bash
docker push registry.gitlab.com/templatemonster/plasmaplatform/services/service-loyalty-programs/php7-fpm/loc:1.0.0
docker push registry.gitlab.com/templatemonster/plasmaplatform/services/service-loyalty-programs/php7-fpm/loc:latest
```
