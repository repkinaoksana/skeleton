FROM php:7-fpm

RUN echo "deb http://ftp.debian.org/debian stable main contrib non-free\ndeb-src http://ftp.debian.org/debian stable main contrib non-free\ndeb http://ftp.debian.org/debian/ jessie-updates main contrib non-free\ndeb-src http://ftp.debian.org/debian/ jessie-updates main contrib non-free\ndeb http://security.debian.org/ jessie/updates main contrib non-free\ndeb-src http://security.debian.org/ jessie/updates main contrib non-free" > /etc/apt/sources.list
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get --yes install wget &&  wget -q -O - https://dl.google.com/linux/linux_signing_key.pub |  apt-key add -

# Install modules
RUN apt-get update && apt-get install -y \
    git \
    libicu-dev \
    libssl-dev \
    libpng12-dev \
    libsasl2-dev \
    librabbitmq4 \
    libmcrypt-dev \
    librabbitmq-dev \
    libmemcached-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    postgresql-server-dev-9.4 \
    --no-install-recommends

RUN apt-get --yes remove curl libcurl3-gnutls
RUN apt-get --yes --force-yes install libcurl3-gnutls=7.38.0-4+deb8u5 libgnutls-deb0-28   --no-install-recommends
RUN apt-mark hold libcurl3-gnutls
RUN apt-get --yes --force-yes install libcurl3=7.38.0-4+deb8u5  curl=7.38.0-4+deb8u5 --no-install-recommends
RUN apt-get --yes install git

RUN docker-php-ext-install bcmath mcrypt intl mbstring pdo_mysql exif \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd \
    && docker-php-ext-configure pgsql \
    && docker-php-ext-install pgsql pdo_pgsql

RUN pecl install -o -f xdebug mongodb \
    && docker-php-ext-enable mongodb \
    && rm -rf /tmp/pear

COPY ./php.ini /usr/local/etc/php/
COPY ./www.conf /usr/local/etc/php-fpm.d/


RUN cd /root \
    && git clone https://github.com/php-memcached-dev/php-memcached \
    && cd php-memcached \
    && git checkout -b php7 origin/php7 \
    && phpize \
    && ./configure \
    && make \
    && make install

RUN echo "extension=memcached.so\nzend_extension=opcache.so" > /usr/local/etc/php/conf.d/memcached.ini

# Install New Relic daemon
RUN apt-get update && \
    apt-get -yq install wget && \
    wget -O - https://download.newrelic.com/548C16BF.gpg | apt-key add - && \
    echo "deb http://apt.newrelic.com/debian/ newrelic non-free" >> /etc/apt/sources.list.d/newrelic.list

RUN apt-get update && \
    apt-get -yq install newrelic-php5
ADD run.sh /run.sh
RUN chmod +x /run.sh

RUN apt-get purge -y g++ \
    && apt-get autoremove -y \
    && rm -r /var/lib/apt/lists/* \
    && rm -rf /tmp/*

RUN pecl install amqp && echo "extension=amqp.so" >> /usr/local/etc/php/conf.d/amqp.ini

EXPOSE 9000
CMD ["php-fpm"]
