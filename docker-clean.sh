#!/bin/bash
docker rm -v $(docker ps -a -q)
docker rmi $(docker images -f "dangling=true" -q)
docker volume ls -qf dangling=true | xargs -r docker volume rm
