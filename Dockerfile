FROM registry.gitlab.com/templatemonster/plasmaplatform/services/service-skeleton/cli:0.0.2

COPY ./docker/build/php.cli.ini /usr/local/etc/php/php.ini
COPY --chown=www-data:www-data ./ssh/ /var/www/.ssh
COPY --chown=www-data:www-data . /app
RUN chmod 0600 /var/www/.ssh/id_rsa \
    && chmod 0600 /var/www/.ssh/id_rsa.pub
RUN cd /app ; composer install
