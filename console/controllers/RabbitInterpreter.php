<?php

namespace console\controllers;

use webtoucher\amqp\components\AmqpInterpreter;
use PhpAmqpLib\Channel\AMQPChannel;

class RabbitInterpreter extends AmqpInterpreter
{
    /**
     * Interprets AMQP message with routing key 'test'.
     *
     * @param array $data
     * @param AMQPChannel $channel
     * @param string $deliveryTag
     *
     * @return boolean|null
     */
    public function readTest($data, $channel, $deliveryTag)
    {
        //Method name after word "read" must be the same as routing key name
        echo 'Hello World!';

        //approves that message was delivered
        $channel->basic_ack($deliveryTag);
    }
}