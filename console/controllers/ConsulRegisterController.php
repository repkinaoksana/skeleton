<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\components\consul\models\Service;
use common\components\consul\services\ServiceRegistry;

class ConsulRegisterController extends Controller
{
    public function actionRegister($name, $id = '', $port = '', $version = '')
    {
        try {
            $service = new Service;
            $service->setName($name);
            if (!empty($id)) {
                $service->setId($id);
            }
            if (!empty($port)) {
                $service->setPort($port);
            }
            if (!empty($version)) {
                $service->addTag('v' . $version);
            }
            $registry = new ServiceRegistry;
            $registry->register($service);
            return self::EXIT_CODE_NORMAL;
        } catch (\Exception $e) {
            return self::EXIT_CODE_ERROR;
        }
    }

    public function actionUnregister($name, $id = '', $port = '', $version = '')
    {
        try {
            $service = new Service;
            $service->setName($name);
            if (!empty($id)) {
                $service->setId($id);
            }
            if (!empty($port)) {
                $service->setPort($port);
            }
            if (!empty($version)) {
                $service->addTag('v' . $version);
            }
            $registry = new ServiceRegistry;
            $registry->unregister($service);
            return self::EXIT_CODE_NORMAL;
        } catch (\Exception $e) {
            return self::EXIT_CODE_ERROR;
        }
    }
}
