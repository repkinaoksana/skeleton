<?php

namespace console\controllers;

use yii\console\Exception;
use yii\helpers\Inflector;
use yii\helpers\Json;
use PhpAmqpLib\Message\AMQPMessage;
use webtoucher\amqp\components\AmqpInterpreter;
use webtoucher\amqp\controllers\AmqpListenerController as AmqpBaseListenerController;


class AmqpListenerController extends AmqpBaseListenerController
{
    public function callback(AMQPMessage $msg)
    {
        $routingKey = $msg->delivery_info['routing_key'];
        $method = 'read' . Inflector::camelize($routingKey);

        if (!isset($this->interpreters[$this->exchange])) {
            $interpreter = $this;
        } elseif (class_exists($this->interpreters[$this->exchange])) {
            $interpreter = new $this->interpreters[$this->exchange];
            if (!$interpreter instanceof AmqpInterpreter) {
                throw new Exception(sprintf("Class '%s' is not correct interpreter class.", $this->interpreters[$this->exchange]));
            }
        } else {
            throw new Exception(sprintf("Interpreter class '%s' was not found.", $this->interpreters[$this->exchange]));
        }

        if (method_exists($interpreter, $method)) {
            $interpreter->$method(
                Json::decode($msg->body, true),
                $msg->delivery_info['channel'],
                $msg->delivery_info['delivery_tag']
            );
        } else {
            if (!isset($this->interpreters[$this->exchange])) {
                $interpreter = new AmqpInterpreter();
            }
            $interpreter->log(
                sprintf("Unknown routing key '%s' for exchange '%s'.", $routingKey, $this->exchange),
                $interpreter::MESSAGE_ERROR
            );
        }
    }
}