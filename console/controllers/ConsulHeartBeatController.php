<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\components\consul\models\Service;
use common\components\consul\services\ServiceHeartBeat;

class ConsulHeartBeatController extends Controller
{

    const EXIT_CODE_CONSUL_HEARTBEAT_FAIL = 2;

    public $defaultAction = 'respond';

    public function actionRespond($name, $id = '')
    {
        $service = new Service;
        $service->setName($name);
        if (!empty($id)) {
            $service->setId($id);
        }
        $heartBeat = new ServiceHeartBeat(null, null, null, $service);
        $heartBeat->addHealthCheck([$this, 'checkDb']);
        $heartBeat->addHealthCheck([$this, 'checkMemcached']);
        try {
            $health = $heartBeat->respond();
        } catch (\Exception $e) {
            $health = false;
        }
        if ($health) {
            return self::EXIT_CODE_NORMAL;
        }
        return self::EXIT_CODE_CONSUL_HEARTBEAT_FAIL;
    }

    public function checkDb()
    {
        try {
            $connection = Yii::$app->db;
            $connection->open();
            if ($connection->pdo !== null) {
                return true;
            }
        } catch (\Exception $e) {
        }
        return false;
    }

    public function checkMemcached()
    {
        try {
            $cache = Yii::$app->cache;
            return $cache->set('healthcheck', 1);
        } catch (\Exception $e) {
        }
        return false;
    }
}
