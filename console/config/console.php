<?php
return [
    'id' => 'console',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'command-bus' => [
            'class' => 'trntv\bus\console\BackgroundBusController',
        ],
        'message' => [
            'class' => 'console\controllers\ExtendedMessageController'
        ],
//        'migrate' => [
//            'class' => 'yii\console\controllers\MigrateController',
//            'migrationPath' => '@common/migrations/db',
//            'migrationTable' => '{{%system_db_migration}}'
//        ],
        'migrate' => [
            'class' => 'indigerd\migrationaware\controllers\MigrateController',
            'configFiles' => [
                '@api/config/web.php',
            ],

            'migrationPath' => '@common/migrations/db', //leave as it was before
            'migrationTable' => '{{%system_db_migration}}' //leave as it was before
        ],
        'rbac-migrate' => [
            'class' => 'console\controllers\RbacMigrateController',
            'migrationPath' => '@common/migrations/rbac/',
            'migrationTable' => '{{%system_rbac_migration}}',
            'templateFile' => '@common/rbac/views/migration.php'
        ],
        'rabbit' => [
            'class' => 'console\controllers\AmqpListenerController',
            'interpreters' => [
                getenv('RABBITMQ_DEFAULT_EXCHANGE') => 'console\controllers\RabbitInterpreter',
            ],
            'exchange' => getenv('RABBITMQ_DEFAULT_EXCHANGE'),
        ],
    ],
    'components' => [
        'amqp' => [
            'class' => 'common\components\amqp\Amqp',
            'host' => getenv('RABBITMQ_DEFAULT_HOST'),
            'port' => getenv('RABBITMQ_DEFAULT_PORT'),
            'user' => getenv('RABBITMQ_DEFAULT_USER'),
            'password' => getenv('RABBITMQ_DEFAULT_PASS'),
            'vhost' => '/',
        ],
    ],
    'params' => [
        'rabbitMQExchange' => getenv('RABBITMQ_DEFAULT_EXCHANGE'),
        'rabbitMQRoutingKey' => getenv('RABBITMQ_DEFAULT_ROUTING_KEY'),
        'rabbitMQQueue' => getenv('RABBITMQ_DEFAULT_QUEUE'),
    ],
];
