<?php

namespace tests\codeception\api;

use tests\codeception\api\FunctionalTester;

use common\components\Blowfish;
use tests\codeception\common\models\TestUser as User;
use api\modules\api\v1\models\UserToken;
use cheatsheet\Time;

class UserCest
{
    protected $users;

    public function setup(FunctionalTester $I)
    {
        $this->users = require(YII_APP_BASE_PATH . '/tests/codeception/common/fixtures/data/user_acceptance.php');
    }

    protected function addToken($i, $user)
    {
        /** @var User $user */
        $tokenInfo = [
            'owner_id' => $user->id,
            'owner_type' => 'user',
            'access_token' => 'token',
            'client_id' => '1',
            'scopes' => [
                'users' => 'users',
                'user-auth-clients' => 'user-auth-clients'
            ]
        ];
        /** @var FunctionalTester $i */
        $i->haveHttpHeader('X-Token-info', json_encode($tokenInfo));
    }

    public function testLookupUser(FunctionalTester $I)
    {
        $I->haveHttpHeader('Accept', 'application/json');
        $I->sendGET('/users/lookup/' . $this->users[0]['login']);
        $response = $I->grabResponse();
        $responseData = \yii\helpers\Json::decode($response, false);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->assertEquals($responseData->login, $this->users[0]['login'], 'login equals expected');
    }
}
