<?php

namespace tests\codeception\api;

use tests\codeception\api\FunctionalTester;

use common\components\Blowfish;
use tests\codeception\common\models\TestUser as User;
use api\modules\api\v1\models\UserToken;
use cheatsheet\Time;

class UserCest
{

//    protected $users;
//
//    public function setup(FunctionalTester $I)
//    {
//        $I->loadFixtures();
//        $this->users = $this->getUsers();
//    }
//
//    protected function getUsers()
//    {
//        return require(YII_APP_BASE_PATH . '/tests/codeception/common/fixtures/data/user.php');
//    }
//
//    protected function addToken($i, $user)
//    {
//        /** @var User $user */
//        $tokenInfo = [
//            'owner_id'     => $user->id,
//            'owner_type'   => 'user',
//            'access_token' => 'token',
//            'client_id'    => '1',
//            'scopes'       => [
//                'users'             => 'users',
//                'user-auth-clients' => 'user-auth-clients'
//            ]
//        ];
//        /** @var FunctionalTester $i*/
//        $i->haveHttpHeader('X-Token-info', json_encode($tokenInfo));
//    }
//
//    public function testRequestResetPassword(FunctionalTester $I)
//    {
//        $id = $this->users[0]['id'];
//        $user = User::find()->where(['id' => $id])->one();
//        $I->haveHttpHeader('Accept', 'application/json');
//        $I->sendGET('/users/request-reset-password/' . $user->id);
//        $response = $I->grabResponse();
//        $I->seeResponseCodeIs(200);
//    }
//
//    public function testResetPassword(FunctionalTester $I)
//    {
//        $newPassword     = 'newpass';
//        $blowfish        = new Blowfish(\Yii::$app->params['tmSalt']);
//        $id              = $this->users[0]['id'];
//        $user            = User::find()->where(['id' => $id])->one();
//        $tokenRepository = UserToken::find();
//        $token           = $tokenRepository->createUserToken($user->id, UserToken::TYPE_PASSWORD_RESET, Time::SECONDS_IN_A_DAY);
//        $resetToken      = $token->token;
//        $I->haveHttpHeader('Accept', 'application/json');
//        $I->sendPUT('/users/reset-password/' . $resetToken .'/'. $newPassword);
//        $response = $I->grabResponse();
//        //echo '----'.$response.'-----';exit;
//        $I->seeResponseCodeIs(200);
//        $user = User::find()->where(['id' => $id])->one();
//        $I->assertEquals($newPassword, $blowfish->decryptString($user->password), 'password changed in DB');
//    }
//
//    public function testAuthClient(FunctionalTester $I)
//    {
//        $testData = [
//            'email' => 'test1-auth-client@email.com',
//            'id'    => 321,
//            'scope' => 'users',
//        ];
//        $I->haveHttpHeader('Accept', 'application/json');
//        $I->sendPOST('/users/auth-client/facebook', $testData);
//        $response = $I->grabResponse();
//        //echo '----'.$response.'-----';exit;
//        $I->seeResponseCodeIs(200);
//        $I->seeResponseIsJson();
//        $responseData = \yii\helpers\Json::decode($response, false);
//        $I->assertNotEmpty($responseData->id);
//        $I->assertEquals($responseData->login, $testData['email']);
//        $I->assertNotEmpty($responseData->token);
//    }
//
//    public function testLogin(FunctionalTester $I)
//    {
//        $id = $this->users[0]['id'];
//        $user = User::find()->where(['id' => $id])->one();
//        $this->addToken($I, $user);
//        $I->haveHttpHeader('Accept', 'application/json');
//        $I->sendPOST('/users/login', ['login' => $this->users[0]['login'], 'password' => 'password']);
//        $response = $I->grabResponse();
//        $I->seeResponseIsJson();
//    }
//
//    public function testGetUser(FunctionalTester $I)
//    {
//        $id = $this->users[0]['id'];
//        $user = User::find()->where(['id' => $id])->one();
//        $this->addToken($I, $user);
//        $I->haveHttpHeader('Accept', 'application/json');
//        $I->sendGET('/users/' . $user->id);
//        $response = $I->grabResponse();
//        $responseData = \yii\helpers\Json::decode($response, false);
//        $I->seeResponseCodeIs(200);
//        $I->seeResponseIsJson();
//        $I->assertEquals($responseData->id, $user->id, 'id equals expected');
//        $I->assertEquals($responseData->login, $user->login, 'login equals expected');
//        $I->assertEquals($responseData->firstname, $user->firstname, 'firstname equals expected');
//        $I->assertEquals($responseData->lastname, $user->lastname, 'lastname equals expected');
//    }
//
//    public function testLookupUser(FunctionalTester $I)
//    {
//        $id = $this->users[0]['id'];
//        $user = User::find()->where(['id' => $id])->one();
//        $I->haveHttpHeader('Accept', 'application/json');
//        $I->sendGET('/users/lookup/' . $user->login);
//        $response = $I->grabResponse();
//        $responseData = \yii\helpers\Json::decode($response, false);
//        $I->seeResponseCodeIs(200);
//        $I->seeResponseIsJson();
//        $I->assertEquals($responseData->login, $user->login, 'login equals expected');
//    }
//
//
//    public function testGetUsers(FunctionalTester $I)
//    {
//        $id = $this->users[0]['id'];
//        $user = User::find()->where(['id' => $id])->one();
//        $users = User::find()->asArray()->all();
//        $this->addToken($I, $user);
//        $I->haveHttpHeader('Accept', 'application/json');
//        $I->sendGET('/users');
//        $response = $I->grabResponse();
//        $responseData = \yii\helpers\Json::decode($response);
//        foreach ($responseData as $k => $row) {
//            if (isset($responseData[$k]['_links'])) unset($responseData[$k]['_links']);
//        }
//        foreach ($users as $k => $row) {
//            unset($users[$k]['password']);
//            unset($users[$k]['countchangeemailstate']);
//            unset($users[$k]['project_id']);
//        }
//        $I->seeResponseCodeIs(200);
//        $I->seeResponseIsJson();
//        $I->assertEquals($responseData, $users, 'Users from db is equal to responce');
//        //print_r($responseData);exit;
//    }
//
//    public function testPostUser(FunctionalTester $I)
//    {
//        $testData = [
//            'login' => 'test3@email.com',
//        ];
//        $user = User::find()->where([
//            'login' => $testData['login'],
//        ])->asArray()->one();
//        $I->assertEquals(null, $user, 'User not exists before api call');
//        $I->haveHttpHeader('Accept', 'application/json');
//        $I->sendPOST('/users', $testData);
//        $response = $I->grabResponse();
//        //echo 'R:'.$response;exit;
//        $responseData = \yii\helpers\Json::decode($response);
//        $I->seeResponseCodeIs(201);
//        $I->seeResponseIsJson();
//        $I->assertEquals($responseData['login'], $testData['login'], 'login equals expected');
//        if (isset($responseData['_links'])) unset($responseData['_links']);
//        $user = User::find()->where([
//            'login' => $testData['login'],
//        ])->asArray()->one();
//        if (isset($user['password'])) unset($user['password']);
//        if (isset($user['project_id'])) unset($user['project_id']);
//        if (isset($user['countchangeemailstate'])) unset($user['countchangeemailstate']);
//        $I->assertEquals($responseData, $user, 'User inserted to db is equal to responce');
//    }
//
//    public function testPutUser(FunctionalTester $I)
//    {
//        $newData = [
//            'firstname' => 'Fnamenew'
//        ];
//        $id = $this->users[0]['id'];
//        $user = User::find()->where(['id' => $id])->one();
//        $this->addToken($I, $user);
//        $I->haveHttpHeader('Accept', 'application/json');
//        $I->sendPUT('/users/' . $user->id, $newData);
//        $response = $I->grabResponse();
//        $responseData = \yii\helpers\Json::decode($response, false);
//        $I->seeResponseCodeIs(200);
//        $I->seeResponseIsJson();
//        $I->assertEquals($responseData->id, $user->id, 'id equals expected');
//        $I->assertEquals($responseData->firstname, $newData['firstname'], 'firstname equals expected');
//        $user = User::find()->where(['id' => $id])->one();
//        $I->assertEquals($responseData->firstname, $user->firstname, 'firstname changed in DB expected');
//    }
}
