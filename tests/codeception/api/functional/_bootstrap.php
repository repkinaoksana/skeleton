<?php
require(__DIR__ . '/../_bootstrap.php');

// Environment
require(YII_APP_BASE_PATH . '/common/env.php');

//die('111');
// Bootstrap application
require(YII_APP_BASE_PATH . '/common/config/bootstrap.php');

$config = require(dirname(dirname(__DIR__)) . '/config/api/functional.php');

new yii\web\Application($config);