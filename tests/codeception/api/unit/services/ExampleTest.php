<?php

namespace tests\codeception\api;

use Codeception\Test\Unit;
use api\modules\api\v1\services\Service;
use yii\base\InvalidParamException;

class ExampleTest extends Unit
{
    /**
     * @var \tests\codeception\api\UnitTester
     */
    protected $tester;

    /** @var Service|\Prophecy\Prophecy\ObjectProphecy */
    protected $service;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function setup()
    {
        $this->service = new Service();
    }

    // tests
    public function testMe()
    {
        $result = $this->service->getItem('testId');

        $this->assertArrayHasKey('id', $result);
    }

    public function testMeFail()
    {
        $this->expectException(InvalidParamException::class);
        $this->expectExceptionMessage('Id is empty');
        $this->expectExceptionCode(500);

        $result = $this->service->getItem(null);
    }
}
