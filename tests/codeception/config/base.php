<?php
/**
 * Application configuration shared by all applications and test types
 */
return [
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\faker\FixtureController',
            'fixtureDataPath' => '@tests/common/fixtures/data',
            'templatePath' => '@tests/common/templates/fixtures',
            'namespace' => 'tests\common\fixtures',
        ],
    ],
    'components' => [
        'db' => [
            'dsn' => getenv('DB_TEST_DSN'), //'mysql:host=localhost;port=3306;dbname=test_db',
            'username' => getenv('DB_TEST_USERNAME'),
            'password' => getenv('DB_TEST_PASSWORD')
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
    ],
    'modules' => [
        'authfilter' => [
            'class' => 'indigerd\oauth2\authfilter\Module',
            'testMode' => true
        ]
    ],
    'params' => [
        'rabbitMQExchange' => 'test',
        'rabbitMQRoutingKey' => 'test',
        'rabbitMQQueue' => 'test',
    ],
];
