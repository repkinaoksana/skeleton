<?php
/**
 * Application configuration shared by all applications acceptance tests
 */
return [
    'homeUrl' => Yii::getAlias('@apiUrl'),
];
