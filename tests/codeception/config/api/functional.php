<?php
$_SERVER['SCRIPT_FILENAME'] = FRONTEND_ENTRY_FILE;
$_SERVER['SCRIPT_NAME'] = FRONTEND_ENTRY_URL;

/**
 * Application configuration for frontend functional tests
 */
Yii::setAlias('@serviceAuthUrl', getenv('AUTH_API_URL'));
return yii\helpers\ArrayHelper::merge(
    require(YII_APP_BASE_PATH . '/api/config/base.php'),
    require(YII_APP_BASE_PATH . '/api/config/web.php'),
    require(dirname(__DIR__) . '/base.php'),
    require(dirname(__DIR__) . '/web.php'),
    require(dirname(__DIR__) . '/functional.php')
);