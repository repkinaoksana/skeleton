<?php
$testEntryUrl = getenv('API_URL') . '/index.php';
$url = parse_url($testEntryUrl, PHP_URL_PATH);
$file = YII_APP_BASE_PATH . '/api/web/index.php';

$_SERVER['SCRIPT_FILENAME'] = $file;
$_SERVER['SCRIPT_NAME'] = $url;
$_SERVER['SERVER_NAME'] = parse_url($testEntryUrl, PHP_URL_HOST);
$_SERVER['SERVER_PORT'] = parse_url($testEntryUrl, PHP_URL_PORT) ?: '80';

/**
 * Application configuration for frontend functional tests
 */
return yii\helpers\ArrayHelper::merge(
    require(YII_APP_BASE_PATH . '/api/config/base.php'),
    require(YII_APP_BASE_PATH . '/api/config/web.php'),
    //require(dirname(__DIR__) . '/base.php'),
    //require(dirname(__DIR__) . '/web.php'),
    require(dirname(__DIR__) . '/acceptance.php')
);
