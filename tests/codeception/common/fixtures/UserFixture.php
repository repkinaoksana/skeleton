<?php

namespace tests\codeception\common\fixtures;

use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture
{
    public $modelClass = 'api\modules\api\v1\models\User';
}
