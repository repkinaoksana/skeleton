<?php

namespace tests\codeception\common\models;

use Yii;

class TestUser extends \api\modules\api\v1\models\User
{
    /**
     * @inheritdoc
     */
    public static function getDb()
    {
        return Yii::$app->db;
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->state_id = static::STATE_WITHOUT_CONFIRM;
            if (empty($this->password)) {
                $this->password = Yii::$app->security->generateRandomString(40);
            }
        }
        if ($insert and !empty($this->password)) {
            $this->hashPassword();
        } elseif (!empty($this->dirtyAttributes['password'])) {
            $this->hashPassword();
        }
        return parent::beforeSave($insert);
    }
}
