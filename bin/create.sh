#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.config.sh"

if [ "$#" -ne 1 ]; then
    colorMsgWarning "No type TEST defined"
    sectionHeader "Documentation build, generate, run tests ..."
    colorMsgInfo "1. Create project local    : make create project"
    colorMsgInfo "1. Create project dev env  : make create dev"
    colorMsgInfo "1. Create project beta env : make create beta"
fi

case "$1" in
    ###################################
    ## Create project
    ###################################
    "project")
        if [ ! -f "$ROOT_DIR/FIRST_INSTALL" ]; then
            logMsg "Create new project $1"
            execInDir "$ROOT_DIR" "composer global require \"fxp/composer-asset-plugin\""
            execInDir "$ROOT_DIR" "composer global require \"squizlabs/php_codesniffer=*\""
            execInDir "$ROOT_DIR" "touch FIRST_INSTALL"
            execInDir "$ROOT_DIR" "cp -f docker-compose.local.yml docker-compose.yml"
            execInDir "$ROOT_DIR" "cp -f vhost.conf.docker.dist vhost.conf"
            execInDir "$ROOT_DIR" "cp -f .env.dist .env"
        fi
        ;;
    ###################################
    ## Create project dev
    ###################################
    "dev")
        if [ ! -f "$ROOT_DIR/FIRST_INSTALL" ]; then
            logMsg "Create new project $1"
            execInDir "$ROOT_DIR" "touch FIRST_INSTALL"
            execInDir "$ROOT_DIR" "cp -f docker-compose.dev.yml docker-compose.yml"
            execInDir "$ROOT_DIR" "cp -f vhost.conf.docker.dist vhost.conf"
            execInDir "$ROOT_DIR" "cp -f .env.dist .env"
        fi
        ;;
    ###################################
    ## Create project beta
    ###################################
    "beta")
        if [ ! -f "$ROOT_DIR/FIRST_INSTALL" ]; then
            logMsg "Create new project $1"
            execInDir "$ROOT_DIR" "touch FIRST_INSTALL"
            execInDir "$ROOT_DIR" "cp -f docker-compose.beta.yml docker-compose.yml"
            execInDir "$ROOT_DIR" "cp -f vhost.conf.docker.dist vhost.conf"
            execInDir "$ROOT_DIR" "cp -f .env.dist .env"
        fi
        ;;
    ###################################
    ## GIT //todo Example
    ###################################
    "git")
        if [ "$#" -lt 2 ]; then
            echo "Missing git url"
            exit 1
        fi
        git clone --recursive "$2" "$CODE_DIR"
        ;;
esac
