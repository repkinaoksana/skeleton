#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.config.sh"

if [ "$#" -ne 1 ]; then
    colorMsgWarning "No type TEST defined"
    sectionHeader "Documentation build, generate, run tests ..."
    colorMsgInfo "1. Run all functional tests:  make test functional"
    colorMsgInfo "2. Run all acceptance tests:  make test acceptance"
    colorMsgInfo "3. Run all unit tests:        make test unit"
    colorMsgInfo "4. Run all tests:             make test all"
    colorMsgInfo "5. Build testing environment: make test build"
    colorMsgInfo "6. Generate unit test:        docker-compose run appsetup vendor/bin/codecept g:test unit Example -c /app/tests/codeception/api"
    colorMsgInfo "7. Generate functional test:  docker-compose run appsetup vendor/bin/codecept g:cest functional Example -c /app/tests/codeception/api"
    exit 0
fi

function migrateDb {
    sectionHeader "RUN migrations ..."
    colorMsgWarning " * Start common migrations"
    docker-compose run appsetup php tests/codeception/bin/yii migrate --migrationPath=@api/modules/api/migrations --interactive=0
    docker-compose run appsetup php tests/codeception/bin/yii mongodb-migrate --migrationPath=@api/modules/api/migrations/mongodb --interactive=0
    colorMsgWarning " * Start tests migrations"
    docker-compose run appsetup php tests/codeception/bin/yii migrate --migrationPath=@tests/tests/codeception/common/migrations --interactive=0
    docker-compose run appsetup php tests/codeception/bin/yii mongodb-migrate --migrationPath=@tests/tests/codeception/common/migrations/mongodb --interactive=0
}

case "$1" in
    ###################################
    ## Run all functional tests
    ###################################
    "functional")
        migrateDb
        sectionHeader "RUN $@ tests ..."
        docker-compose run appsetup vendor/bin/codecept run functional -c /app/tests/codeception/api
        logMsg "FINISH $@ tests"
        ;;
    ###################################
    ## Run all acceptance tests
    ###################################
    "acceptance")
        migrateDb
        sectionHeader "RUN $@ tests ..."
        docker-compose run appsetup vendor/bin/codecept run acceptance -c /app/tests/codeception/api
        logMsg "FINISH $@ tests"
        ;;
    ###################################
    ## Run all unit tests
    ###################################
    "unit")
        sectionHeader "RUN $@ tests ..."
        docker-compose run appsetup vendor/bin/codecept run unit -c /app/tests/codeception/api
        logMsg "FINISH $@ tests"
        ;;
    ###################################
    ## Run all tests
    ###################################
    "all")
        migrateDb
        sectionHeader "RUN $@ tests ..."
        docker-compose run appsetup vendor/bin/codecept run -c /app/tests/codeception/api
        sectionHeader "FINISH tests."
        ;;
    ###################################
    ## Generate config test environment
    ###################################
    "build")
        sectionHeader "$@ tests ..."
        docker-compose run appsetup vendor/bin/codecept build -c /app/tests/codeception/api
        sectionHeader "FINISH build."
        ;;
    "coverage")
        sectionHeader "$@ ..."
        docker-compose run appsetup vendor/bin/codecept run unit -c /app/tests/codeception/api --coverage --coverage-html
        sectionHeader "FINISH $@ tests"
        ;;
esac