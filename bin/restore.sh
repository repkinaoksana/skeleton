#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.config.sh"

if [ "$#" -ne 1 ]; then
    echo "No type defined"
    exit 1
fi

mkdir -p -- "${BACKUP_DIR}"

case "$1" in
    ###################################
    ## MySQL REVIEWS
    ###################################
    "mysql")
        DB=$(docker exec "$(dockerContainerId db)" sh -c 'echo "${MYSQL_DATABASE}"')
        if [[ -n "$(dockerContainerId db)" ]]; then
            if [ -f "${BACKUP_DIR}/${DB}.${BACKUP_MYSQL_FILE}" ]; then
                logMsg "Starting MySQL restore..."
                bzcat "${BACKUP_DIR}/${DB}.${BACKUP_MYSQL_FILE}" | docker exec -i "$(dockerContainerId db)" sh -c 'exec mysql -u$MYSQL_USER -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE"'
                logMsg "Finished"
            else
                errorMsg "MySQL backup file not found"
                exit 1
            fi
        else
            colorMsgWarning " * Skipping mysql backup, no such container \"db\""
        fi
        ;;
    ###################################
    ## MySQL TEST REVIEWS
    ###################################
    "test")
        DB=$(docker exec "$(dockerContainerId db)" sh -c 'echo "${MYSQL_DATABASE2}"')
        if [[ -n "$(dockerContainerId db)" ]]; then
            if [ -f "${BACKUP_DIR}/${DB}.${BACKUP_MYSQL_FILE}" ]; then
                logMsg "Starting MySQL restore..."
                bzcat "${BACKUP_DIR}/${DB}.${BACKUP_MYSQL_FILE}" | docker exec -i "$(dockerContainerId db)" sh -c 'exec mysql -u$MYSQL_USER -p"$MYSQL_PASSWORD" "$MYSQL_DATABASE2"'
                logMsg "Finished"
            else
                errorMsg "MySQL backup file not found"
                exit 1
            fi
        else
            colorMsgWarning " * Skipping mysql backup, no such container \"db\""
        fi
        ;;
esac
