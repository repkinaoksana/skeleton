#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value


#######################################
## Configuration
#######################################

READLINK='readlink'
unamestr=`uname`
if [ "$unamestr" == 'FreeBSD' -o "$unamestr" == 'Darwin'  ]; then
  READLINK='greadlink'
fi

if [ -z "`which $READLINK`" ]; then
    echo "[ERROR] $READLINK not installed"
    echo "        make sure coreutils are installed"
    echo "        MacOS: brew install coreutils"
    exit 1
fi

SCRIPT_DIR=$(dirname "$($READLINK -f "$0")")
ROOT_DIR=$($READLINK -f "$SCRIPT_DIR/../")
CODE_DIR=$($READLINK -f "$ROOT_DIR")

BACKUP_DIR=$($READLINK -f "$ROOT_DIR/backup")
BACKUP_SOLR_FILE='solr.cores.tbz2'
BACKUP_MYSQL_FILE='mysql.sql.bz2'

MYSQL_DATABASES='api'

#######################################
## Color Schema
#######################################
COLOR_NC='\e[0m' # No Color
COLOR_WHITE='\e[1;37m'
COLOR_BLACK='\e[0;30m'
COLOR_BLUE='\e[0;34m'
COLOR_LIGHT_BLUE='\e[1;34m'
COLOR_GREEN='\e[0;32m'
COLOR_LIGHT_GREEN='\e[1;32m'
COLOR_CYAN='\e[0;36m'
COLOR_LIGHT_CYAN='\e[1;36m'
COLOR_RED='\e[0;31m'
COLOR_LIGHT_RED='\e[1;31m'
COLOR_PURPLE='\e[0;35m'
COLOR_LIGHT_PURPLE='\e[1;35m'
COLOR_BROWN='\e[0;33m'
COLOR_YELLOW='\e[1;33m'
COLOR_GRAY='\e[0;30m'
COLOR_LIGHT_GRAY='\e[0;37m'

COLOR_INFO=`echo "\033[m"`
#COLOR_PRIMARY=`echo "\033[36m"` #Blue
#CODE_WARNING=`echo "\033[33m"` #yellow
#COLOR_FGRED=`echo "\033[41m"`
COLOR_DANGER=`echo "\033[31m"`
COLOR_SUCCESS=`echo "\033[33m"`
colorHeaderInfo(){
    echo -e "${COLOR_GRAY}$*${COLOR_NC}"
}
colorMsgInfo(){
    echo -e "${COLOR_CYAN}$*${COLOR_NC}"
}
colorMsgDanger(){
    echo -e "${COLOR_RED}$*${COLOR_NC}"
}
colorMsgSuccess(){
    echo -e "${COLOR_GREEN}$*${COLOR_NC}"
}
colorMsgWarning(){
    echo -e "${COLOR_YELLOW}$*${COLOR_NC}"
}

#######################################
## Functions
#######################################

errorMsg() {
    colorMsgDanger "[ERROR] $*"
}

logMsg() {
    colorMsgInfo " * $*"
}

sectionHeader() {
    colorHeaderInfo "*** $* ***"
}

execInDir() {
    echo "[RUN :: $1] $2"

    sh -c "cd \"$1\" && $2"
}

dockerContainerId() {
    echo "$(docker-compose ps -q "$1" 2> /dev/null || echo "")"
}

dockerExec() {
    docker exec -i "$(docker-compose ps -q app)" $@
}

dockerExecShCommand() {
    docker exec "$(docker-compose ps -q $1)" sh -c $2
}

dockerCopyFrom() {
    PATH_DOCKER="$1"
    PATH_HOST="$2"
    docker cp "$(docker-compose ps -q app):${PATH_DOCKER}" "${PATH_HOST}"
}
dockerCopyTo() {
    PATH_HOST="$1"
    PATH_DOCKER="$2"
    docker cp "${PATH_HOST}" "$(docker-compose ps -q app):${PATH_DOCKER}"
}
# todo Перевірити можливість заміни схожих строк на функцію
pathToBackupMysqlDb() {
    echo "${BACKUP_DIR}/$@.${BACKUP_MYSQL_FILE}"
}

dockerCreateMysqlDb() {
    docker exec $(dockerContainerId mysql) sh -c 'exec echo "CREATE DATABASE IF NOT EXISTS '$@' CHARACTER SET utf8 COLLATE utf8_unicode_ci;" | mysql -uroot -p"$MYSQL_ROOT_PASSWORD"'
}

dockerRestoreMysqlDb() {
    bzcat "${BACKUP_DIR}/$@.${BACKUP_MYSQL_FILE}" | docker exec -i "$(dockerContainerId mysql)" sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD" '$@
}

dockerBackupMysqlDb() {
    docker exec  "$(dockerContainerId mysql)" sh -c 'exec mysqldump --opt --single-transaction --events --routines --comments -uroot -p"$MYSQL_ROOT_PASSWORD" '"$@" | bzip2 > "${BACKUP_DIR}/$@.${BACKUP_MYSQL_FILE}"
}