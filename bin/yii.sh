#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.config.sh"

if [ "$#" -ne 1 ]; then
    colorMsgWarning "No type Yii COMMAND defined"
    sectionHeader "Documentation run yii2 commands ..."
    colorMsgInfo "1. Show Yii2 commands:    make yii help"
    colorMsgInfo "2. Run setup:             make yii setup"
    colorMsgInfo "3. Run migration:         make yii migrate"
    colorMsgInfo "4. Run redoing migration: make yii migrate-redo"
    colorMsgInfo "4. For create migration: docker-compose run appsetup console/yii migrate/create add_example_table --migrationPath=@api/modules/api/migrations --interactive=0"
    colorMsgInfo "4. For create mongo migration: docker-compose run appsetup console/yii mongodb-migrate/create add_example_collection --migrationPath=@api/modules/api/migrations/mongodb --interactive=0"
    exit 0
fi

# todo Здесь описать yii команды, например
case "$1" in
    ###################################
    ## Run help yii commands
    ###################################
    "help")
        sectionHeader "RUN $@ ..."
        docker-compose run appsetup console/yii help
        logMsg "FINISH $@"
        ;;
    ###################################
    ## Run setup application
    ###################################
    "setup")
        sectionHeader "RUN yii $@ ..."
        docker-compose run appsetup console/yii app/setup
        logMsg "FINISH $@"
        ;;
    ###################################
    ## Run mysql migrations
    ###################################
    "migrate")
        sectionHeader "RUN $@ ..."
#        docker-compose run appsetup console/yii migrate --migrationPath=@api/modules/api/migrations --interactive=0
        docker-compose run appsetup console/yii mongodb-migrate --migrationPath=@api/modules/api/migrations/mongodb --interactive=0
        logMsg "FINISH $@"
        ;;
    ###################################
    ## Run mysql redoing migrations
    ###################################
    "migrate-redo")
        sectionHeader "RUN $@ ..."
        docker-compose run appsetup console/yii migrate/redo --migrationPath=@api/modules/api/migrations --interactive=0
        logMsg "FINISH $@"
        ;;
#    ###################################
#    ## Run mysql migrations
#    ###################################
#    "migrate-create")
#        sectionHeader "RUN $@ ..."
#        docker-compose run appsetup console/yii migrate/create $2 --migrationPath=@api/modules/api/migrations --interactive=0
#        logMsg "FINISH $@"
#        ;;
esac