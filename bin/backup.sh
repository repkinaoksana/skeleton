#!/usr/bin/env bash

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   ## set -u : exit the script if you try to use an uninitialised variable
set -o errexit   ## set -e : exit the script if any statement returns a non-true return value

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/.config.sh"

if [ "$#" -ne 1 ]; then
    colorMsgWarning "No type defined"
    exit 1
fi

mkdir -p -- "${BACKUP_DIR}"

case "$1" in
    ###################################
    ## MySQL
    ###################################
    "mysql")
        if [[ -n "$(dockerContainerId db)" ]]; then
            DATABASES=$(docker exec "$(dockerContainerId db)" sh -c 'echo "${MYSQL_DATABASE}" "${MYSQL_DATABASE2}"')
            for DB in ${DATABASES}; do
                if [[ -n "$(docker exec "$(dockerContainerId db)" sh -c 'exec mysql -u$MYSQL_USER -p"$MYSQL_PASSWORD" -e "show databases" | grep '${DB})" ]]; then
                    if [ -f "${BACKUP_DIR}/${DB}.${BACKUP_MYSQL_FILE}" ]; then
                        logMsg "Removing old backup file..."
                        rm -f -- "${BACKUP_DIR}/${DB}.${BACKUP_MYSQL_FILE}"
                    fi
                    logMsg "Starting MySQL backup DB: ${DB}..."
                    dockerBackupMysqlDb ${DB}
                else
                    colorMsgWarning " * Skipping mysql backup, no DB"
                fi
            done

            logMsg "Finished"
        else
            colorMsgWarning " * Skipping mysql backup, no such container \"db\""
        fi
        ;;
    ###################################
    ## MongoDB
    ###################################
    "mongodb")
        colorMsgWarning " * Skipping mongodb backup, no script for backup"
        ;;
esac
