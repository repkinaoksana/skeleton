#!/bin/bash


cp docker-compose-local.yml docker-compose.yml
cp -f .env.dist .env
docker-compose pull
docker-compose build --no-cache
docker-compose down -v
export COMPOSE_HTTP_TIMEOUT=600
docker-compose up -d
docker-compose logs -f appsetup


docker-compose run appsetup ./functional.sh
docker-compose run appsetup ./unit.sh

echo "For project management use docker-compose (stop , start , ps, logs) in current dir"

