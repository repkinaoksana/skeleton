<?php

namespace common\commands;

use yii\base\Object;
use yii\swiftmailer\Message;
use trntv\bus\interfaces\SelfHandlingCommand;
use common\modules\authfilter\client\Curl as Client;

class SendEmailCommand extends Object implements SelfHandlingCommand
{
    /**
     * @var mixed
     */
    public $from;
    /**
     * @var mixed
     */
    public $to;
    /**
     * @var string
     */
    public $subject;
    /**
     * @var string
     */
    public $view;
    /**
     * @var array
     */
    public $params;
    /**
     * @var string
     */
    public $body;
    /**
     * @var bool
     */
    public $html = true;

    /**
     * Command init
     */
    public function init()
    {
        $this->from = $this->from ?: \Yii::$app->params['robotEmail'];
    }

    /**
     * @return bool
     */
    public function isHtml()
    {
        return (bool)$this->html;
    }

    /**
     * @param \common\commands\SendEmailCommand $command
     * @return bool
     */
    public function handle($command)
    {
        return $this->{'handle' . ucfirst($command->params['emailType'])}($command);
        /*
        if (!$command->body) {
            $message = \Yii::$app->mailer->compose($command->view, $command->params);
        } else {
            $message = new Message();
            if ($command->isHtml()) {
                $message->setHtmlBody($command->body);
            } else {
                $message->setTextBody($command->body);
            }
        }
        $message->setFrom($command->from);
        $message->setTo($command->to ?: \Yii::$app->params['robotEmail']);
        $message->setSubject($command->subject);
        return $message->send();
        */
    }

    /**
     * @TODO inject urls via params
     */
    protected function handleReset($command)
    {
        $params = json_encode([
            'email' => $command->to,
            'letter_name' => 'Retrieve password templatemonster',
            'projectName' => 'templatemonster',
            'macros' => [
                'resetPasswordLink' => getenv('MAIL_SERVICE_RESET_URL') . '?hash=' . $command->params['token'],
                'userName' => '',
            ]
        ]);
        return $this->sendToQueue($params);
    }

    protected function handleRegistration($command)
    {
        $params = json_encode([
            'email' => $command->to,
            'letter_name' => 'Registration templatemonster',
            'projectName' => 'templatemonster',
            'macros' => [
                'confirmUrl' => getenv('MAIL_SERVICE_CONFIRM_URL') . '?hash=' . $command->params['token'],
                'userName' => '',
            ]
        ]);
        return $this->sendToQueue($params);
    }

    protected function sendToQueue($params)
    {
        $client = new Client;
        try {
            $url = getenv('MAIL_SERVICE_URL') . '?user=' . getenv('MAIL_SERVICE_USER') . '&token=' . getenv('MAIL_SERVICE_TOKEN');
            $result = $client->sendRequest('POST', $url, $params);
            //echo $result->content;exit;
            return $result;
        } catch (\Exception $e) {
        }
    }
}
