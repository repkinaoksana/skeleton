<?php

namespace common\components;

class Blowfish
{

    private $key;

    private $cipher;

    public function __construct($pass)
    {
        $this->cipher = mcrypt_module_open('blowfish', '', 'cbc', '');
        $this->key = pack('H*', sha1($pass));
    }

    public function encryptString($plaintext, $iv = '')
    {
        if ($iv == '') {
            $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($this->cipher));
        } else {
            $iv = pack("H*", $iv);
        }
        mcrypt_generic_init($this->cipher, $this->key, $iv);
        $bs = mcrypt_enc_get_block_size($this->cipher);
        $plaintext = mb_convert_encoding($plaintext, 'UTF-16BE');
        $pkcs = $bs - (strlen($plaintext) % $bs);
        $pkcs = str_repeat(chr($pkcs), $pkcs);
        $plaintext = $plaintext . $pkcs;
        $result = mcrypt_generic($this->cipher, $plaintext);
        mcrypt_generic_deinit($this->cipher);
        return bin2hex($iv . $result);
    }

    public function decryptString($ciphertext)
    {
        $ciphertext = pack("H*", $ciphertext);
        $bs = mcrypt_enc_get_block_size($this->cipher);
        $iv_size = mcrypt_enc_get_iv_size($this->cipher);

        if ((strlen($ciphertext) % $bs) != 0) {
            return false;
        }
        $iv = substr($ciphertext, 0, $iv_size);
        $ciphertext = substr($ciphertext, $iv_size);
        mcrypt_generic_init($this->cipher, $this->key, $iv);
        $result = mdecrypt_generic($this->cipher, $ciphertext);
        $padding = ord(substr($result, -1));
        $result = substr($result, 0, $padding * -1);
        mcrypt_generic_deinit($this->cipher);
        $result = mb_convert_encoding($result, 'UTF-8', 'UTF-16BE');
        return $result;
    }

    public function __destruct()
    {
        mcrypt_module_close($this->cipher);
    }
}
