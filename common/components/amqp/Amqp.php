<?php

namespace common\components\amqp;

use webtoucher\amqp\components\Amqp as AmqpBase;

class Amqp extends AmqpBase
{
    /**
     * @inheritdoc
     */
    public function listen($exchange, $routing_key, $callback, $type = Amqp::TYPE_TOPIC)
    {
        $queueName = \Yii::$app->params['rabbitMQQueue'];
        $this->channel->basic_consume($queueName, '', false, false, false, false, $callback);

        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }

        $this->channel->close();
        $this->connection->close();
    }
}