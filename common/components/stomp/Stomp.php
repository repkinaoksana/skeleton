<?php

namespace common\components\stomp;

use yii\base\Component;
use Stomp\Client;
use Stomp\StatefulStomp;
use Stomp\Transport\Message;

class Stomp extends Component
{
    public $dsn;
    public $queue;

    /**
     * @var StatefulStomp
     */
    protected $stomp;

    /**
     * @return StatefulStomp
     */
    public function getStomp()
    {
        if (null === $this->stomp) {
            $this->stomp = new StatefulStomp(
                new Client($this->dsn)
            );
        }
        return $this->stomp;
    }

    public function send($message, $queue = '')
    {
        $queue = $queue ?: $this->queue;
        $this->getStomp()->send($queue, new Message($message));
    }
}
