<?php
// Path aliases
Yii::setAlias('@base', realpath(__DIR__ . '/../../'));
Yii::setAlias('@common', realpath(__DIR__ . '/../../common'));
Yii::setAlias('@console', realpath(__DIR__ . '/../../console'));
Yii::setAlias('@api', realpath(__DIR__ . '/../../api'));
Yii::setAlias('@tests', realpath(__DIR__ . '/../../tests'));

// Url Aliases
Yii::setAlias('@apiUrl', getenv('API_URL'));
Yii::setAlias('@apiTestUrl', getenv('API_TEST_URL'));

