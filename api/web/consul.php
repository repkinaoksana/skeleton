<?php
// Composer
require(__DIR__ . '/../../vendor/autoload.php');

require(__DIR__ . '/../../common/env.php');

// Yii
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');

// Bootstrap application
require(__DIR__ . '/../../common/config/bootstrap.php');

$sd = new \common\components\consul\services\ServiceDiscovery(null, null, 'http://178.62.206.231:8500');
$authUrl = $sd->getServiceAddress('service-auth');
echo 'URL:' . $authUrl;
