<?php
// NOTE: Make sure this file is not accessible when deployed to production
//if (!in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1' , '172.17.0.1' ])) {
//    die('You are not allowed to access this file.');
//}
error_reporting(E_ALL);
// TEST ENV
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'test');
defined('YII_APP_BASE_PATH') or define('YII_APP_BASE_PATH', dirname(dirname(__DIR__)));

// Bootstraping tests environment
require(__DIR__ . '/../../tests/codeception/api/_bootstrap.php');

// Environment
require(__DIR__ . '/../../common/env.php');

// Bootstrap application
require(__DIR__ . '/../../common/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');

$config = require(__DIR__ . '/../../tests/codeception/config/api/functional.php');


(new yii\web\Application($config))->run();
