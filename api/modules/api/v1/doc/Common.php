<?php

/**
 * @apiDefine AuthRequired
 *
 * @apiHeader {String} Authorization Access token. Use either the Authorization header or the access_token query param to pass an access token.
 *
 * @apiParam {String} access_token Access token. Use either the Authorization header or the access_token query param to pass an access token.
 *
 */

/**
 * @apiDefine NotFoundError
 *
 * @apiError NotFound The id of the Item was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "name":"Not Found","message":"Object not found: 1234","code":404,"status":404
 *     }
 */

/**
 * @apiDefine ValidationError
 *
 * @apiError ValidationError Invalid data provided for item save.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 422 Data Validation Failed.
 *     [{"field":"user_id","message":"User Id cannot be blank."}]
 */

/**
 * @apiDefine BadRequestError
 *
 * @apiError BadRequestError Bad request.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request.
 *     {
 *       "name":"Bad Request","message":"Wrong token","code":400,"status":400
 *     }
 */

