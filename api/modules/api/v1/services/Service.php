<?php

namespace api\modules\api\v1\services;

use yii\base\InvalidParamException;
use yii\web\Request;
use yii\data\ActiveDataProvider;

class Service
{
    public function getItems($modelClass, Request $request)
    {
        $searchModel = new $modelClass;
        /** @var ActiveDataProvider $dataProvider */
        $dataProvider = $searchModel->search($request->queryParams);
        $perPage = $request->getQueryParam('per-page');
        if ($perPage == '0') {
            $dataProvider->pagination = false;
        } else {
            $p = $dataProvider->getPagination();
            $p->setPageSize((int)$perPage);
        }
        return $dataProvider;
    }

    public function getItem($id)
    {
        if ($id === null) {
            throw new InvalidParamException('Id is empty', 500);
        }

        return [
            'id' => $id
        ];
    }
}
