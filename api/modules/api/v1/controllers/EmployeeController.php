<?php

namespace api\modules\api\v1\controllers;

use indigerd\rest\Controller;
use yii\web\UnprocessableEntityHttpException;

class EmployeeController extends Controller
{
    public $modelClass = '';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['except'] = [
            'options',
            'index',
            'view',
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);

        return $actions;
    }

    public function actionIndex()
    {
        throw new UnprocessableEntityHttpException('Test action');
    }
}
