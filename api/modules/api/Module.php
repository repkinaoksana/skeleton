<?php

namespace api\modules\api;

use Yii;
use yii\base\Module as BaseModule;
use indigerd\migrationaware\MigrationAwareInterface;

/**
 * Class Module
 * @package api\modules\api
 * @property string $migrationPath
 */
class Module extends BaseModule implements MigrationAwareInterface
{
    public function getMigrationPath()
    {
        return __DIR__ . '/migrations';
    }

    public function init()
    {
        parent::init();
    }
}
