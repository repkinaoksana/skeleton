<?php
return [
    'components' => [
        'urlManager' => require(__DIR__ . '/_urlManager.php'),
        'cache' => [
            'class' => 'yii\caching\DummyCache'
        ],
    ],
];
