<?php
return [
    'id' => 'api',
    'basePath' => dirname(__DIR__),
    'name' => 'Yii2 API Proof of Concept',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'sourceLanguage' => 'en-US',
    'language' => 'en-US',
    'components' => [
        'urlManager' => require(__DIR__ . '/_urlManager.php'),
        'cache' => [
            'class' => 'yii\caching\MemCache',
            'useMemcached' => true,
            'servers' => [
                [
                    'host' => getenv('MEMCACHE_HOST'),
                    'port' => getenv('MEMCACHE_PORT'),
                    'weight' => 100,
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => getenv('DB_DSN'),
            'username' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
            'tablePrefix' => getenv('DB_TABLE_PREFIX'),
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
            'schemaCache' => 'cache',
        ],
        'user' => [
            'identityClass' => 'indigerd\oauth2\authfilter\identity\AuthServerIdentity',
            'enableSession' => false
        ],
        'amqp' => [
            'class' => 'webtoucher\amqp\components\Amqp',
            'host' => getenv('RABBITMQ_DEFAULT_HOST'),
            'port' => getenv('RABBITMQ_DEFAULT_PORT'),
            'user' => getenv('RABBITMQ_DEFAULT_USER'),
            'password' => getenv('RABBITMQ_DEFAULT_PASS'),
            'vhost' => '/',
        ],
    ],
    'modules' => [
        'authfilter' => [
            'class' => 'indigerd\oauth2\authfilter\Module',
            'authServerUrl' => Yii::getAlias('@serviceAuthUrl'),
            'clientId' => getenv('AUTH_CLIENT_ID'),
            'clientSecret' => getenv('AUTH_CLIENT_SECRET')
        ]
    ],
    'params' => [
        'rabbitMQExchange' => getenv('RABBITMQ_DEFAULT_EXCHANGE'),
        'rabbitMQRoutingKey' => getenv('RABBITMQ_DEFAULT_ROUTING_KEY'),
        'rabbitMQQueue' => getenv('RABBITMQ_DEFAULT_QUEUE'),
    ],
];
