ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent

list:
	sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

#############################
# Create new project
#############################

create:
	bash bin/create.sh $(ARGS)

#############################
# Docker machine states
#############################

up:
	docker-compose up -d

start:
	docker-compose start

stop:
	docker-compose stop

state:
	docker-compose ps

rebuild:
	docker-compose stop
	docker-compose rm --force cli
	docker-compose rm --force appsetup
	docker-compose rm --force nginx
	docker-compose rm --force fpm
	docker-compose build --no-cache
	docker-compose up -d

restart:
	docker-compose stop
	docker-compose start

logs:
	docker-compose logs

clear:
	docker rm $$(docker ps -a | grep -Eo "serviceskeleton_appsetup_run_[0-9]{1,3}")

remove:
	docker-compose down

#############################
# MySQL
#############################

mysql-backup:
	bash ./bin/backup.sh mysql

mysql-restore:
	bash ./bin/restore.sh mysql

#############################
# test
#############################

test:
	bash ./bin/test.sh $(ARGS)

#############################
# Make doc
#############################

doc:
	apidoc -i api/ -o api/web/doc/ -f api/modules/api/v1/doc/

#############################
# General
#############################

backup: mysql-backup

restore: mysql-restore

build:
	cp -f vhost.conf.docker.dist vhost.conf
	cp -f .env.dist .env
	cp -f docker-compose.es.local.yml docker-compose.yml
	bash bin/build.sh

clean:
	test -d app/typo3temp && { rm -rf app/typo3temp/*; }

bash: shell

shell:
	docker exec -it -u application $$(docker-compose ps -q app) /bin/bash

root:
	docker exec -it -u root $$(docker-compose ps -q app) /bin/bash

#############################
# Argument fix workaround
#############################
%:
	@:
